package serverCustom;

import java.util.ArrayList;

public class ListClient {
	
	private ArrayList <Client> listClient;
	
	public ListClient() {
		listClient = new ArrayList<Client>();
	}
	
	public ArrayList <Client> getListClient() {
		return listClient;
	}
	
	public void setListClient(ArrayList <Client> listClient) {
		this.listClient = listClient;
	}
}
