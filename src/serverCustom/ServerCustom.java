package serverCustom;

import java.net.*;
import java.io.*;

public class ServerCustom {

	static ListClient listClient;

	public static void main(String[] args) {

		try (ServerSocket serverSocket = new ServerSocket(9000)) {

			System.out.println("Server is listening on port 9000");
			listClient = new ListClient();

			while (true) {
				Socket socket = serverSocket.accept();
				System.out.println("New client connected");

				InputStream input = socket.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(input));

				OutputStream output = socket.getOutputStream();
				PrintWriter writer = new PrintWriter(output, true);

				String text;

				do {
					text = reader.readLine();

					String ans = actionDispatcher(text);

					if (ans != null) {
						writer.println("Server: " + ans);
					}

				} while (!text.equals("DONE"));

				socket.close();
			}

		} catch (IOException ex) {
			System.out.println("Server exception: " + ex.getMessage());
			ex.printStackTrace();
		}

	}

	private static String actionDispatcher(String text) {

		String action = "Command incorrect";

		if (text.matches("REGISTER\s[a-zA-Z]+\s[a-zA-Z]+")) {
			String[] arr = text.split(" ", 2);
			Client client = new Client(arr[1]);

			System.out.println("Client created : " + client.getName());
			listClient.getListClient().add(client);

			return client.getId();

		} else if (text.matches("AGE\s.{36}\s[1-9]+")) {
			String[] arr = text.split(" ", 3);

			for (Client c : listClient.getListClient()) {
				if (c.getId().equals(arr[1])) {
					c.setAge(Integer.parseInt(arr[2]));
					System.out.println(c.getId() + ", Age = " + c.getAge());
				}
			}

			return null;
		} else if (text.matches("COUNTRY\s.{36}\s[a-zA-Z]+")) {
			String[] arr = text.split(" ", 3);

			for (Client c : listClient.getListClient()) {
				if (c.getId().equals(arr[1])) {
					c.setCountry(arr[2]);
					System.out.println(c.getId() + ", Country = " + c.getCountry());
				}
			}

			return null;
		} else if (text.matches("UNREGISTER\s.{36}")) {
			String[] arr = text.split(" ", 2);
			for (Client c : listClient.getListClient()) {
				if (c.getId().equals(arr[1])) {
					listClient.getListClient().remove(c);
				}
			}
		} else if (text.equals("LIST")) {
			String clientList = "";
			for (Client c : listClient.getListClient()) {
				clientList += c.getId() + ":name=" + c.getName() + ",age=" + c.getAge() + ",country=" + c.getCountry();
			}
			return clientList;
		}

		return action;
	}
}
